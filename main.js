const { app, BrowserWindow, ipcMain } = require('electron');
const { join } = require('path');

app.disableHardwareAcceleration()

let win;
const createWindow = () => {
    win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            sandbox: false,
            preload: join(__dirname, 'preload.js'),
        },
    });
    win.loadFile('app/index.html');
};
app.whenReady().then(() => {
    createWindow();
});

ipcMain.on('notification', (event, args) => {
    win.webContents.send("notification", args);
});
