const DB = require('./db');
const { v4: uuid } = require('uuid');
const path = require('path');
const puppeteer = require('puppeteer-extra');
const stealthPlugin = require('puppeteer-extra-plugin-stealth');
const fs = require('fs/promises');
const PNGCrop = require('png-crop');
const imageUtils = require('./imageUtils');

puppeteer.use(stealthPlugin());

let browser;

module.exports = class Controller {
    static async getItems() {
        return await DB.get();
    }

    static async deleteItem(itemId) {
        let items = await DB.get();
        await DB.set(items.filter(i => i.id !== itemId));
    }

    static async addItem(data) {
        const db = await DB.get();
        if (db.every(item => item.url !== data.url)) {
            db.push({ ...data, id: uuid() });
        }
        await DB.set(db);
    }

    static async crawlItems() {
        !browser && (browser = await puppeteer.launch({ 
            headless: true, 
            args: ['--no-sandbox'],
            defaultViewport: { width: 1500, height: 1200 } 
        }));
        const page = await browser.newPage();
    
        const items = await DB.get();
        for (let item of items) {
            const { url, selector, id } = item;
            const imageCompare = imageUtils.getImageCompare(id);
    
            await page.goto(url);
            await new Promise(resolve => setTimeout(resolve, 15*1000));
    
            if (selector) {
                const el = await page.$(selector);
                if (!el) {
                    console.log('Could not find the element.');
                    continue;
                }
                await el.screenshot({ path: imageCompare.getCurrentImagePath() });
            } else {
                await page.screenshot({ path: imageCompare.getCurrentImagePath(), fullPage: true });
            }

            if (await imageCompare.compare()) {
                item.lastUpdate = new Date();
            }
        }
    
        await page.close();
        await DB.set(items);
    }
};
