const fs = require('fs/promises');
const path = require('path');

class DB {
    static get #dbPath() {
        return path.join(__dirname, '..', 'data', 'db.json');
    }

    static async get() {
        const content = await fs.readFile(this.#dbPath, { encoding: 'utf-8' });
        return JSON.parse(content || '[]');
    }

    static async set(obj) {
        await fs.writeFile(this.#dbPath, JSON.stringify(obj, null, 2), { encoding: 'utf-8' });
    }
}
module.exports = DB;
