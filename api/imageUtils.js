const path = require('path');
const fs = require('fs/promises');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const PNGCrop = require('png-crop');
const _ = require('lodash');
const { v4: uuid } = require('uuid');

module.exports = class imageUtils {
    static _DIFFERENCE_PIXEL_THRESHOLD = 500;

    static get _imageDirectory() {
        return path.join(__dirname, '..', 'data', 'img');
    }

    static _decodeImageName(imageName) {
        const segments = imageName.split('_');
        const isDifferenceOutput = segments[0] === 'diff';
        if (isDifferenceOutput) {
            return {
                itemId: segments[1],
                isDifferenceOutput
            };
        } else if (segments.length === 2) {
            return {
                itemId: segments[1],
                timestamp: new Date(parseInt(segments[0]))
            };
        }
        return { itemId: segments[0] };
    }

    static _encodeImageName({
        itemId, timestamp, isDifferenceOutput
    }) {
        if (isDifferenceOutput) {
            return `diff_${itemId}`;
        } else if (timestamp) {
            return `${timestamp.getTime()}_${itemId}`;
        }
        return `${itemId}`;
    }

    static async _cropImage(sourcePath, destinationPath, height, width) {
        await new Promise(async (resolve, reject) => {
            PNGCrop.crop(sourcePath, destinationPath, { height, width }, (err) => {
                if (err) reject(err);
                resolve();
            });
        });
    }

    static getImageCompare(itemId) {
        return new imageUtils({ itemId });
    }

    _itemId = null;
    _images = null;
    _currentImage = null;
    _mostRecentImage = null;
    _temporaryFiles = [];

    /**
     * @private
     */
    constructor({ itemId }) {
        this._itemId = itemId;
    }

    getCurrentImagePath() {
        const imageConfig = {
            itemId: this._itemId
        };
        return this._buildImagePath(imageConfig);
    }

    /**
     * @returns {Promise<boolean>} Is a different image.
     */
    async compare() {
        const images = await this._getImages();
        const mostRecentImageInfo = _.chain(images)
            .map(image => imageUtils._decodeImageName(image.split('.')[0]))
            .filter(imageInfo => imageInfo.itemId === this._itemId && !imageInfo.isDifferenceOutput && imageInfo.timestamp)
            .sort(imageInfo => imageInfo.timestamp.getTime())
            .last()
            .value();
        if (!mostRecentImageInfo) {
            return this._handleDifference();
        }
        this._currentImage = PNG.sync.read(await fs.readFile(this.getCurrentImagePath()));
        this._mostRecentImage = PNG.sync.read(await fs.readFile(this._buildImagePath(mostRecentImageInfo)));
        if (this._areImagesOfMatchingSize() === false) {
            await this._cropImages({ mostRecentImageInfo, ...this._getCropSize() });
        }
        if (await this._compare()) {
            return this._handleDifference();
        } else {
            return this._handleNoDifference();
        }
    }

    async _compare() {
        const { width, height } = this._currentImage;
        const diff = new PNG({ width, height });
        const pixels = pixelmatch(
            this._currentImage.data, 
            this._mostRecentImage.data, 
            diff.data, 
            width, 
            height, 
            { threshold: 0.1 }
        );
        if (pixels > imageUtils._DIFFERENCE_PIXEL_THRESHOLD) {
            const differenceImageConfig = {
                itemId: this._itemId,
                isDifferenceOutput: true
            };
            await fs.writeFile(this._buildImagePath(differenceImageConfig), PNG.sync.write(diff));
            return true;
        }
        return false;
    }

    async _getImages() {
        if (this._images === null) {
            this._images = await fs.readdir(imageUtils._imageDirectory);
        }
        return this._images;
    }

    _buildImagePath(imageInfo) {
        return path.join(imageUtils._imageDirectory, imageUtils._encodeImageName(imageInfo) + '.png');
    }

    async _handleNoDifference() {
        await fs.rm(this.getCurrentImagePath());
        await this._deleteTemporaryFiles();
        return false;
    }

    async _handleDifference() {
        const newImageConfig = {
            itemId: this._itemId,
            timestamp: new Date()
        };
        await fs.rename(
            this.getCurrentImagePath(), 
            this._buildImagePath(newImageConfig)
        );
        await this._deleteTemporaryFiles();
        return true;
    }

    async _deleteTemporaryFiles() {
        for (const file of this._temporaryFiles) {
            await fs.rm(file);
        }
    }

    _getCropSize() {
        return {
            width: Math.min(this._currentImage.width, this._mostRecentImage.width),
            height: Math.min(this._currentImage.height, this._mostRecentImage.height),
        };
    }

    _areImagesOfMatchingSize() {
        return this._currentImage.width === this._mostRecentImage.width
            && this._currentImage.height === this._mostRecentImage.height;
    }

    async _cropImages({ width, height, mostRecentImageInfo }) {
        const croppedCurrentImagePath = this._buildImagePath({ itemId: uuid() });
        const croppedMostRecentImagePath = this._buildImagePath({ itemId: uuid() });
        this._temporaryFiles.push(
            croppedCurrentImagePath,
            croppedMostRecentImagePath
        );
        await imageUtils._cropImage(this.getCurrentImagePath(), croppedCurrentImagePath, height, width);
        await imageUtils._cropImage(this._buildImagePath(mostRecentImageInfo), croppedMostRecentImagePath, height, width);
        this._currentImage = PNG.sync.read(await fs.readFile(croppedCurrentImagePath));
        this._mostRecentImage = PNG.sync.read(await fs.readFile(croppedMostRecentImagePath));
    }
}
