const { CronJob: Cron } = require('cron'); 
const { contextBridge, ipcRenderer } = require('electron');
const Controller = require('./api/controller');

let crawlJob = new Cron('0 */5 0-2,8-23 * * *', async () => {
    try {
        await Controller.crawlItems();
    } catch(err) {
        ipcRenderer.send('notification', {
            type: 'error',
            message: err.message,
            stack: err.stack
        });
        console.error(new Date().toLocaleString(), err.message);
        console.error(err);
    }
});
crawlJob.start();

contextBridge.exposeInMainWorld('electron', {
    getItems: async () => Controller.getItems(),
    deleteItem: async (id) => Controller.deleteItem(id),
    addItem: async (data) => Controller.addItem(data),
    ipcListener: (channel, func) => {
        if (channel === 'notification') {
            ipcRenderer.on(channel, (event, ...args) => func(...args));
        }
    }
});
