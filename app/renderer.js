const SECOND_MS = 1000;
const MINUTE_MS = SECOND_MS * 60;
const HOUR_MS = MINUTE_MS * 60;
const DAY_MS = HOUR_MS * 24;

class App {
    static get _refreshMs() {
        return 10*1000;
    }

    static init() {
        electron.ipcListener('notification', (data) => {
            this.showNotification(data);
        });

        document.querySelector('form').addEventListener('submit', async (e) => {
            e.preventDefault();
            await electron.addItem({
                url: document.querySelector('form > input[name=url]').value,
                selector: document.querySelector('form > input[name=selector]').value || null,
            });
            await this.updateTable();
            document.querySelector('form > input[name=url]').value = '';
            document.querySelector('form > input[name=selector]').value = '';
        });
        this.updateTable().then(() => setInterval(() => this.updateTable(), this._refreshMs));
    }

    static timeAgo(lastUpdate) {
        const differenceMs = Date.now() - new Date(lastUpdate).getTime();
        if (differenceMs >= DAY_MS) {
            const days = Math.floor(differenceMs / DAY_MS);
            return `${days} day${days > 1 ? 's':''} ago`;
        } else if (differenceMs >= HOUR_MS) {
            const hours = Math.floor(differenceMs / HOUR_MS);
            return `${hours} hour${hours > 1 ? 's':''} ago`;
        } else if (differenceMs >= MINUTE_MS) {
            const minutes = Math.floor(differenceMs / MINUTE_MS);
            return `${minutes} minute${minutes > 1 ? 's':''} ago`;
        } else {
            const seconds = Math.floor(differenceMs / SECOND_MS);
            return `${seconds} second${seconds > 1 ? 's':''} ago`;
        }
    }

    static deleteItem(id) {
        electron.deleteItem(id).then(() => this.updateTable());
    }

    static async updateTable() {
        let html = '';
        for (let item of await electron.getItems()) {
            let lastUpdate = item.lastUpdate ? this.timeAgo(item.lastUpdate) : '-';
            let lastUpdateMs = item.lastUpdate 
                ? Date.now() - new Date(item.lastUpdate).getTime()
                : undefined;
            html += `<tr ${lastUpdateMs < this._refreshMs ? 'class="highlight"' : ''}>
                <td><a href="${item.url}">${item.url}</a></td>
                <td>${lastUpdate}</td>
                <td><button class="icon-btn" onclick="app.deleteItem('${item.id}')">🗑</button></td>
            </tr>`;
        }
        const tableEl = document.querySelector('table > tbody');
        tableEl.innerHTML = html;
    }

    static showNotification({ type, message }) {
        const notificationEl = document.querySelector('#notification');
        document.querySelector('#notification > span').textContent = message;
        if (type === 'error') 
            notificationEl.classList.add('error'); 
        else 
            notificationEl.classList.remove('error');
        notificationEl.style.display = '';
    }

    static dismissNotification() {
        document.querySelector('#notification').style.display = 'none';
    }
}

App.init();
window.app = App;
